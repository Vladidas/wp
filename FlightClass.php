<?php

/**
 * Class Flight
 * @author Vlad Golubtsov <v.golubtsov@bvblogic.com>
 */
class Flight
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $config;

    /**
     * @var SoapClient
     */
    private $soapClient;

    /**
     * Flight constructor.
     * @param string $url
     * @param array $config
     * @throws SoapFault
     */
    public function __construct(string $url, array $config)
    {
        $this->url        = $url;
        $this->config     = $config;
        $this->soapClient = $this->getSoapClient();
    }

    /**
     * @param $action
     * @param $arguments
     * @return mixed
     */
    public function __call($action, $arguments)
    {
        return $this->soapClient->__call($action, $arguments);
    }

    /**
     * @return SoapClient
     * @throws SoapFault
     */
    public function getSoapClient()
    {
        return new SoapClient($this->url, $this->config);
    }
}
