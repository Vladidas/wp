<?php

require_once 'recaptchalib.php';

$reCaptchaLang    = 'ua';
$reCaptchaSiteKey = '6Ld8K6QUAAAAADe6GNOYtukFLpowT2PmykV5_XMI';
$reCaptchaSecret  = '6Ld8K6QUAAAAAFL7kGgYmlLR-XJzgSiHRgT3pLz6';

$reCaptcha = new ReCaptcha($reCaptchaSecret);

if ($_POST['g-recaptcha-response']) {
    $response = $reCaptcha->verifyResponse(
        $_SERVER['REMOTE_ADDR'],
        $_POST['g-recaptcha-response']
    );
    if ($response != null && $response->success) {
        echo "You got it!";
    }
}
?>

<form action="?" method="post">
    <div class="g-recaptcha" data-sitekey="<?= $reCaptchaSiteKey; ?>"></div>
    <script type="text/javascript"
            src="https://www.google.com/recaptcha/api.js?hl=<?= $reCaptchaLang; ?>">
    </script>
    <br/>
    <input type="submit" value="submit" />
</form>