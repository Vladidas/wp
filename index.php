<?php

$flightAwareUserName = 'kruzovise';
$flightAwareApiKey   = '85d95dbb24048dd66ebe04328771a6546251642c';
$compression         = 1;

$options = [
    'trace'      => true,
    'exceptions' => 0,
    'login'      => $flightAwareUserName,
    'password'   => $flightAwareApiKey,
];

function dump($result) {
    echo '<pre style="background: black; color: greenyellow">';
    print_r($result);
    echo '</pre>';
}

$client = new SoapClient('http://flightxml.flightaware.com/soap/FlightXML2/wsdl', $options);

$time         = time();
$airportBegin = 'KJFK';
$beginOfDay   = $time - 600;
$endOfDay     = $time + 600;

$params = [
    'query'   => '{= orig '.$airportBegin.'} {range actualDepartureTime '.$beginOfDay.' '.$endOfDay.'}',
    'howMany' => 15,
    'offset'  => 0
];
dump($client->SearchBirdseyeInFlight($params));

//$params = ['ident' => 'SWA2558', 'howMany' => 15];
//dump($client->FlightInfo($params));
//
//$params = ['airport' => 'KAUS'];
//dump($client->Metar($params));
//
//$params = [
//    'startDate'   => time() - 10000,
//    'endDate'     => time(),
//    'origin'      => '',
//    'destination' => '',
//    'airline'     => '',
//    'flightno'    => '',
//    'howMany'     => 15,
//    'offset'      => 0
//];
//dump($client->AirlineFlightSchedules($params));

//$params = ['faFlightID' => 'UAL9465'];
//dump($client->AirlineFlightInfo($params));
//
//$params = ['faFlightID' => 'CLH1944'];
//dump($client->AirlineFlightInfo($params));
//
//$params = ['faFlightID' => 'EDDM'];
//dump($client->AirlineFlightInfo($params));
//
//$params = ['faFlightID' => 'LIMJ'];
//dump($client->AirlineFlightInfo($params));
//
//$params = ['faFlightID' => 'CRJ9'];
//dump($client->AirlineFlightInfo($params));

//$params = ['faFlightID' => 'VOE1657'];
//dump($client->GetHistoricalTrack($params));